type DataProp = import("editorjs-blocks-react-renderer").DataProp;

type Nullable<T> = T | null | undefined;

type ArticleResponse = {
  // without "article"
  article_id: number;
  article_index: string; // ? all is 'index'
  article_name: string; // name OR title
  article_title: string; // it is good for links but sometimes it is cyrillic
  author_id: number;
  date: string;
  image_thumbail_url: string; // ? maybe abs path (http://...) now it gets from NEXT_PUBLIC_STATIC_FILES_URL
  is_draft: string; // ? all is 'draft'
  tag_name: string;
  tagslug: string;
  time_to_read: string;
  // important?: boolean - Важное за месяц
  // type?: 'onTop' | 'onBottom'
};

type ArticleDetailResponse = {
  article_id: number;
  content: string;
  // more data from ArticleResponse
};

type ArticleCombinedResponse = ArticleDetailResponse & ArticleResponse;

type AuthorResponse = {
  id: string; // number
  initials: string;
  name: string;
  //? image_thumbail_url for author
};

type TagResponse = {
  display_order: string; // number
  is_enabled: string; // boolean
  tag: string; // tagName: string
  tag_id: string; // number
  tagslug: string; // what is slug?
};

type IModalOpen = {
  open: boolean;
  setOpen: (state: boolean) => void;
};

type ArticleDetail = Omit<ArticleDetailResponse, 'content'> &
  ArticleResponse & { content: DataProp };
