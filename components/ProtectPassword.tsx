import { useEffect, useState } from "react";
import { OP_JOURNAL_CLIENT_LOGGED } from "../utils/constants";

const PROTECT_PASSWORD = "op123!";

const ProtectPassword = () => {
  const [password, setpassword] = useState("");

  useEffect(() => {
    if (password === PROTECT_PASSWORD) {
      window.localStorage.setItem(OP_JOURNAL_CLIENT_LOGGED, "true");
      window.location.reload()
    }
  }, [password]);
  return (
    <form className="ProtectPassword">
      <input
        placeholder="Enter a password"
        type="password"
        className="main-menu__search-input"
        value={password}
        onChange={(e: React.FormEvent<HTMLInputElement>) =>
          setpassword(e.currentTarget.value)
        }
      />
    </form>
  );
};
export default ProtectPassword;
