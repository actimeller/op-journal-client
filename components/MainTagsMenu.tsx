import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import tagStore from "../store/TagStore";
import Link from "./Link";
import { observer } from "mobx-react-lite";

function MainTagsMenu() {
  const router = useRouter();
  const [tags, setTags] = useState<TagResponse[]>();

  useEffect(() => {
    setTags(tagStore.data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tagStore.data]);

  return (
      <div className="tags-menu-holder">
        <div className="tags-menu">
          {tags?.map((tag) => (
              <Link
                  to={`/tag/${tag.tagslug}`}
                  className={`tags-menu__item ${
                      router.query.tagName === tag.tagslug && "is-active"
                  }`}
                  key={tag.tag_id}
              >
                #{tag.tag}
              </Link>
          ))}
        </div>
      </div>
  );
};


export default observer(MainTagsMenu)