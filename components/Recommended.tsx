import Link from "./Link";
import ReactSlick from "react-slick";
import _ from "lodash";
import { opImageUrl } from "../utils";

export default function Recommended({ cards }: { cards: ArticleResponse[] }) {
  const settings = {
    infinite: false,
    easing: 'linear',
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          infinite: true,
          arrows: false,
        }
      }
    ]
  };
  return (
    <section className="article-page-recommended">
      <span className="article-page-recommended__title">Смотрите также:</span>
      <div className="slider-cards article-page-recommended__slider">
        <ReactSlick {...settings}>
          {cards?.map(
            ({
              article_name: title,
              article_id: id,
              image_thumbail_url: imageUrl,
              tag_name:tag
            }) => (
              <div
                key={_.uniqueId()}
                className="slider-cards__slide"
              >
                <Link to={`/article/${id}`} className="slider-card">
                  <div className="slider-card__media">
                    <span
                      className="slider-card__bg"
                      style={{ backgroundImage: `url(${opImageUrl(imageUrl)})`}}
                    />
                  </div>
                  <div className="slider-card__info">
                    <strong className="slider-card__name">
                      {title}
                    </strong>
                    <span className="slider-card__category">{tag}</span>
                  </div>
                </Link>
              </div>
            )
          )}
        </ReactSlick>
      </div>
    </section>
  );
}
