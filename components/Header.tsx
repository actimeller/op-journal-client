import Link from "./Link";
import MainTagList from "./MainTagsMenu";

export default function Header({
  modalOpened,
  setModalOpened,
}: {
  modalOpened: boolean;
  setModalOpened: (opened: boolean) => void;
}) {
  return (
    <header>
      <div className="wrap">
        <div className="header">
          <div className="header__row-mobile">
            <div className="header__item-mobile">
              <div className="header__logo">
                <Link className="header__logo-link" to="/">
                  <sup className="header__logo-shield" />
                  <strong className="header__logo-name" />
                </Link>
              </div>
            </div>
            <div className="header__item-mobile">
              <div className="search header__search">
                <button className="search__btn" />
              </div>
              <div className="user header__user">
                <div className="user__avatar">
                  <span className="user__avatar-pic" />
                </div>
              </div>
              <button
                  type="button"
                  className="mobile-menu__switch"
                  onClick={() => setModalOpened(!modalOpened)}
                  aria-label="open mobile menu"
              >
                <span className="mobile-menu__switch-item" />
              </button>
            </div>
          </div>
          <div className="header__row">
            <div className="header__item">
              <button
                  type="button"
                  className="mobile-menu__switch"
                  onClick={() => setModalOpened(!modalOpened)}
                  aria-label="open mobile menu"
              >
                <span className="mobile-menu__switch-item" />
              </button>
            </div>
            <div className="header__item">
              <MainTagList />
            </div>
            <div className="header__item">
              <div className="search header__search">
                <button className="search__btn" />
              </div>
              <div className="user header__user">
                <div className="user__avatar">
                  <span className="user__avatar-pic" />
                </div>
              </div>
            </div>
          </div>
            <div className="header__logo desktop-only">
              <Link className="header__logo-link" to="/">
                <sup className="header__logo-shield" />
                <strong className="header__logo-name" />
              </Link>
            </div>
        </div>
      </div>
    </header>
  );
}
