import Link from "./Link";
import Image from "next/image";
import _ from "lodash";

// todo remove this mess
import logoViber from "../assets/images/logos/footerContact1.svg";
import logoSkype from "../assets/images/logos/logo-skype.svg";
import logoFacebookMess from "../assets/images/logos/footerContact2.svg";
import logoTelegram from "../assets/images/logos/logo-telegram.svg";
import logoFacebook from "../assets/images/logos/logo-facebook.svg";
import logoVK from "../assets/images/logos/logo-vk.svg";
import logoOPBubble from "../assets/images/logos/footerContact3.svg";
import { useState } from "react";

const FooterMenuItem = ({
  title,
  link,
  isTitle,
  target,
  rel,
}: {
  title: string;
  link: string;
  isTitle?: Boolean;
  target?: string;
  rel?: string;
}) => (
  <li className="footer__menu-item" key={title}>
    {isTitle ? (
      <span
        className="footer__item-title"
        dangerouslySetInnerHTML={{ __html: title }}
      />
    ) : (
      <Link to={link} className="footer__menu-link" target={target} rel={rel}>
        <span dangerouslySetInnerHTML={{ __html: title }} />
      </Link>
    )}
  </li>
);

const FooterDivisionBlock = ({
  data,
}: {
  data: {
    title: string;
    list: { title: string; link: string }[];
  }[];
}) => {
  const [activeBlock, setActiveBlock] = useState<string>();

  return (
    <>
      {data.map(({ title, list }) => (
        <div
          className={`footer__division-block ${
            activeBlock === title && "is-open"
          }`}
          key={title}
        >
          <div
            className="footer__division-name"
            dangerouslySetInnerHTML={{ __html: title }}
            onClick={() =>
              setActiveBlock(activeBlock !== title ? title : undefined)
            }
          />
          <div className="footer__division-sub">
            {list.map((i) => (
              <Link to={i.link} className="footer__division-link" key={i.title}>
                <span dangerouslySetInnerHTML={{ __html: i.title }}></span>
              </Link>
            ))}
          </div>
        </div>
      ))}
    </>
  );
};

export default function Footer() {
  return (
    <footer>
      <div className="wrap">
        <div className="footer mobile-hidden">
          <div className="footer__row footer__row_first">
            <div className="footer__item">
              <div className="footer__item-holder">
                <div className="footer__menu-holder">
                  <ul className="footer__menu">
                    {[
                      { title: "Основные сервисы", link: "/", isTitle: true },
                      { title: "Услуги и\u00a0цены", link: "/" },
                      { title: "Товарные знаки", link: "/" },
                      { title: "Изобретения", link: "/" },
                      { title: "Промышленные образцы", link: "/" },
                      { title: "Программы для\u00a0ЭВМ", link: "/" },
                      { title: "SMM-Броня", link: "/" },
                    ].map((item) => (
                      <FooterMenuItem key={item.title} {...item} />
                    ))}
                  </ul>
                  <ul className="footer__menu">
                    {[
                      { title: "Немного о\u00a0нас", link: "/", isTitle: true },
                      { title: "База знаний", link: "/" },
                      { title: "Блог и\u00a0новости", link: "/" },
                      { title: "Стать партнером", link: "/" },
                      { title: "Документы", link: "/" },
                      { title: "Реквизиты", link: "/" },
                      { title: "Контакты", link: "/" },
                    ].map((item) => (
                      <FooterMenuItem key={item.title} {...item} />
                    ))}
                  </ul>
                </div>
                <div className="footer__menu-holder footer__menu-holder_bottom">
                  <ul className="footer__menu">
                    {[
                      { title: "Мы в\u00a0соцсетях", link: "/", isTitle: true },
                      {
                        title: "YouTube",
                        link: "https://www.youtube.com/c/onlinepatent?_ga=2.263631429.89382938.1586843791-1317663824.1522918962",
                        target: "_blank",
                        rel: "noreferrer",
                      },
                      {
                        title: "Instagram",
                        link: "https://www.instagram.com/online_patent/?_ga=2.263631429.89382938.1586843791-1317663824.1522918962",
                        target: "_blank",
                        rel: "noreferrer",
                      },
                    ].map((item) => (
                      <FooterMenuItem key={item.title} {...item} />
                    ))}
                  </ul>
                  <ul className="footer__menu">
                    {[
                      {
                        title: "Facebook",
                        link: "https://www.facebook.com/ONLINEPATENT?_ga=2.263631429.89382938.1586843791-1317663824.1522918962",
                        target: "_blank",
                        rel: "noreferrer",
                      },
                      {
                        title: "ВКонтакте",
                        link: "https://vk.com/onlinepatentru?_ga=2.204566617.89382938.1586843791-1317663824.1522918962",
                        target: "_blank",
                        rel: "noreferrer",
                      },
                    ].map((item) => (
                      <FooterMenuItem key={item.title} {...item} />
                    ))}
                  </ul>
                </div>
              </div>
            </div>
            <div className="footer__item footer__item_right">
              <div className="footer__item-holder footer__item-holder_right">
                <span className="footer__item-title">
                  Остались вопросы? А мы всегда на&nbsp;связи:
                </span>
                <div className="footer__connect">
                  {[
                    {
                      href: "viber://pa?chatURI=online_patent",
                      imageSrc: logoViber,
                    },
                    {
                      href: "skype:support@onlinepatent.ru?chat",
                      imageSrc: logoSkype,
                    },
                    {
                      href: "https://facebook.com/messages/t/ONLINEPATENT",
                      imageSrc: logoFacebookMess,
                    },
                    {
                      href: "https://telegram.me/OnlinePatentBot",
                      imageSrc: logoTelegram,
                    },
                    {
                      href: "https://facebook.com/messages/t/ONLINEPATENT",
                      imageSrc: logoFacebook,
                    },
                    {
                      href: "https://vk.com/im?sel=-61850364",
                      imageSrc: logoVK,
                    },
                  ].map(
                    ({
                      href,
                      imageSrc,
                    }: {
                      href: string;
                      imageSrc: string;
                    }) => (
                      <Link
                        to={href}
                        rel="nofollow noreferrer"
                        target="_blank"
                        className="footer__connect-block"
                        key={_.uniqueId()}
                      >
                        <Image
                          className="footer__connect-pic"
                          src={imageSrc}
                          alt=""
                        />
                      </Link>
                    )
                  )}
                  <Link
                    to="mailto:support@onlinepatent.ru"
                    className="footer__connect-block footer__connect-block_write"
                  >
                    <span className="footer__connect-text">Написать нам</span>
                    <Image
                      className="footer__connect-pic footer__connect-pic_write"
                      src={logoOPBubble}
                      alt=""
                    />
                  </Link>
                </div>
                <div className="footer__address">
                  <Link to="tel:88003333662" className="footer__address-phone">
                    8 800 333-36-62
                  </Link>
                  <Link to="tel:+74952301745" className="footer__address-phone">
                    +7 495 230-17-45
                  </Link>
                  <Link
                    to="mailto:support@onlinepatent.ru"
                    className="footer__menu-link footer__address-mail"
                  >
                    support@onlinepatent.ru
                  </Link>
                </div>
                <div className="footer__btns">
                  <div className="btn-ps btn-ps_orange contact-us-popup">
                    <span className="btn-ps__arrow" />
                    <span className="btn-ps__text">Заказать звонок</span>
                  </div>
                  <Link
                    to="https://my.onlinepatent.ru/login/registration"
                    rel="nofollow"
                    className="btn-ps btn-ps_blue"
                  >
                    <span className="btn-ps__arrow" />
                    <span className="btn-ps__text">Начать работать</span>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="footer__bottom">
            <div className="footer__row footer__row_bottom">
              <div className="footer__item">
                <div className="footer__item-holder">
                  <div className="footer__skolkovo">
                    <Link
                      to="http://sk.ru/"
                      rel="nofollow"
                      className="footer__skolkovo-logo"
                    />
                    <span className="footer__skolkovo-text">
                      Исследования осуществляются ООО «Онлайн патент»
                      при&nbsp;грантовой поддержке Фонда «Сколково»
                    </span>
                  </div>
                </div>
              </div>
              <div className="footer__item footer__item_right">
                <div className="footer__item-holder footer__item-holder_right">
                  {/* <div className="footer__pay {if $PAGE_18PLUS}footer__pay_18plus{/if}"> */}
                  <div className="footer__pay footer__pay_18plus">
                    <span className="footer__pay-item footer__pay-item_visa" />
                    <span className="footer__pay-item footer__pay-item_pay-pal" />
                    <span className="footer__pay-item footer__pay-item_master" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer desktop-hidden">
          <div className="footer__top">
            <Link to="/" className="footer__logo" />
            <div className="footer__link-block">
              <span className="footer__main-link">Журнал</span>
            </div>
          </div>
          <div className="footer__division">
            <FooterDivisionBlock
              data={[
                {
                  title: "Услуги и&nbsp;цены",
                  list: [
                    { title: "Услуги по&nbsp;товарным знакам", link: "/" },
                    {
                      title: "Услуги по&nbsp;программам для&nbsp;ЭВМ",
                      link: "/",
                    },
                    { title: "Услуги по&nbsp;базам данных", link: "/" },
                    {
                      title: "Услуги по&nbsp;патентам на&nbsp;дизайн",
                      link: "/",
                    },
                    { title: "Услуги по&nbsp;изобретениям", link: "/" },
                  ],
                },
                {
                  title: "Товарные знаки",
                  list: [
                    { title: "Регистрация товарного знака", link: "/" },
                    { title: "Все услуги по&nbsp;товарным знакам", link: "/" },
                    {
                      title: "Бесплатный поиск по&nbsp;товарным знакам",
                      link: "/",
                    },
                    { title: "Порядок регистрации товарного знака", link: "/" },
                    { title: "Справочник МКТУ", link: "/" },
                  ],
                },
                {
                  title: "Программы, софт",
                  list: [
                    { title: "Регистрация программы для&nbsp;ЭВМ", link: "/" },
                    { title: "Все услуги по&nbsp;программам", link: "/" },
                  ],
                },
                {
                  title: "Базы данных",
                  list: [
                    { title: "Регистрация базы данных", link: "/" },
                    { title: "Все услуги по&nbsp;базам данных", link: "/" },
                    { title: "", link: "/" },
                  ],
                },
                {
                  title: "Промышленные образцы",
                  list: [
                    { title: "Оформление патента на&nbsp;дизайн", link: "/" },
                    {
                      title: "Все услуги по&nbsp;патентам на&nbsp;дизайн",
                      link: "/",
                    },
                    { title: "", link: "/" },
                  ],
                },
                {
                  title: "Патенты на&nbsp;изобретения",
                  list: [
                    {
                      title: "Оформление патента на&nbsp;изобретение",
                      link: "/",
                    },
                    { title: "Все услуги по&nbsp;изобретениям", link: "/" },
                    { title: "", link: "/" },
                  ],
                },
                {
                  title: "Общая информация",
                  list: [
                    { title: "База знаний", link: "/" },
                    { title: "Блог и&nbsp;новости", link: "/" },
                    { title: "Отзывы о&nbsp;нас", link: "/" },
                    { title: "Наши партнеры", link: "/" },
                    { title: "О&nbsp;компании", link: "/" },
                  ],
                },
              ]}
            />

            <Link to="/" rel="nofollow" className="btn-ps btn-ps_orange footer__btn">
              <span className="footer__btn-text">
                Попробовать бесплатно прямо&nbsp;сейчас
              </span>
            </Link>
            <div className="footer__connect">
              <span className="footer__connect-title">
                Остались вопросы? А&nbsp;мы&nbsp;всегда на&nbsp;связи:
              </span>
              {[
                {
                  href: "viber://pa?chatURI=online_patent",
                  imageSrc: logoViber,
                },
                {
                  href: "skype:support@onlinepatent.ru?chat",
                  imageSrc: logoSkype,
                },
                {
                  href: "https://facebook.com/messages/t/ONLINEPATENT",
                  imageSrc: logoFacebookMess,
                },
                {
                  href: "https://telegram.me/OnlinePatentBot",
                  imageSrc: logoTelegram,
                },
                {
                  href: "https://facebook.com/messages/t/ONLINEPATENT",
                  imageSrc: logoFacebook,
                },
                { href: "https://vk.com/im?sel=-61850364", imageSrc: logoVK },
              ].map(
                ({ href, imageSrc }: { href: string; imageSrc: string }) => (
                  <Link
                    to={href}
                    rel="nofollow noreferrer"
                    target="_blank"
                    className="footer__connect-block"
                    key={_.uniqueId()}
                  >
                    <Image
                      className="footer__connect-pic"
                      src={imageSrc}
                      alt=""
                    />
                  </Link>
                )
              )}
              <Link
                to="mailto:support@onlinepatent.ru"
                className="footer__connect-block footer__connect-block_write"
              >
                <span className="footer__connect-text">Написать нам</span>
                <Image
                  className="footer__connect-pic footer__connect-pic_write"
                  src={logoOPBubble}
                  alt=""
                />
              </Link>
            </div>
            <div className="footer__address">
              <Link
                to="tel:88003333662"
                className="footer__address-phone footer-text"
              >
                8 800 333-36-62
              </Link>
              <Link
                to="tel:+74952301745"
                className="footer__address-phone footer-text"
              >
                +7 495 230-17-45
              </Link>
              <Link
                to="mailto:support@onlinepatent.ru"
                className="footer__address-mail footer-text"
              >
                support@onlinepatent.ru
              </Link>
            </div>
            <div className="footer__skolkovo">
              <Link
                to="http://sk.ru/"
                rel="nofollow"
                className="footer__skolkovo-logo"
              />
              <span className="footer__skolkovo-text">
                Исследования осуществляются ООО «Онлайн патент»
                при&nbsp;грантовой поддержке Фонда «Сколково»
              </span>
            </div>
            <div className="footer__pay">
              <span className="footer__pay-item footer__pay-item_visa" />
              <span className="footer__pay-item footer__pay-item_pay-pal" />
              <span className="footer__pay-item footer__pay-item_master" />
            </div>
            <ul className="footer__menu">
              {[
                { title: "Документы", link: "/" },
                { title: "Реквизиты", link: "/" },
                { title: "База знаний", link: "/" },
                { title: "Контакты", link: "/" },
              ].map((item) => (
                <FooterMenuItem key={item.title} {...item} />
              ))}
            </ul>
          </div>
        </div>
      </div>
      <div className="footer-subscribe-holder">
        <div className="footer-subscribe">
          <div className="wrap">
            <span className="footer-subscribe__title">
              Подпишитесь на&nbsp;полезную рассылку:
            </span>
            <form className="footer-subscribe__form">
              <div className="form__input-block">
                <input
                  type="text"
                  className="form__input footer-subscribe__input"
                  placeholder="Введите свой e-mail"
                />
              </div>
              <div className="footer-subscribe__btn">
                <a href="#" className="btn-ps btn-ps_white">
                  <span className="btn-ps__arrow btn-ps__arrow_black" />
                  <span className="btn-ps__text">Подписаться</span>
                </a>
              </div>
            </form>
            <label className="checkbox footer-subscribe__checkbox">
              <input type="checkbox" defaultChecked className="tick" />
              <span className="footer-subscribe__agreement-text">
                Я принимаю
                <a href="#" target="_blank">
                  условия передачи информации
                </a>
              </span>
            </label>
          </div>
        </div>
      </div>
    </footer>
  );
}
