import Head from "next/head";
import { useEffect, useState } from "react";
import _ from "lodash";

import Modal from "./MainMenuModal";
import Header from "../components/Header";
import Footer from "../components/Footer";
import MainTagList from "./MainTagsMenu";

interface MainContainerProps {
  ogTitle?: string;
  ogUrl?: string;
  ogImg?: string;
  ogDescription?: string;
  children: React.ReactNode;
}

export default function MainContainer({
  ogTitle,
  ogUrl,
  ogImg,
  ogDescription,
  children,
}: MainContainerProps) {
  const [mobileMenuOpened, setMobileMenuOpened] = useState(false);

  useEffect(() => {
    document.body.classList[!mobileMenuOpened ? "remove" : "add"](
      "menu-is-open",
      "padding-right-15",
      "modal-open"
    );
  }, [mobileMenuOpened]);

  return (
    <>
      <Head>
        <title>Online patent journal</title>
        <meta name="format-detection" content="telephone=no, address=no" />
        <meta
          name="viewport"
          content="width=device-width, height=device-height, initial-scale=1.0"
        />

        <meta name="msvalidate.01" content="4A4A6D56145049E06BB920E976F8AE50" />
        <meta
          name="wmail-verification"
          content="f445057031b334b2769d73b3b9e56707"
        />
        <meta name="mailru-verification" content="4cab912f060a1302" />
        <meta name="wot-verification" content="f2c972161baaff587a83" />

        <meta
          name="geo.placename"
          content="Россия, Москва, Бережковская наб., д. 6"
        />
        <meta name="geo.position" content="55.740117;37.565865" />
        <meta name="geo.region" content="RU" />
        <meta name="ICBM" content="55.740117, 37.565865" />

        <meta property="og:title" content={ogTitle} />
        <meta property="og:url" content={ogUrl} />
        <meta property="og:image" content={ogImg} />
        <meta property="og:description" content={ogDescription} />
        <meta property="og:site_name" content="www.onlinepatent.ru" />

        <meta name="robots" content="noyaca" />
        <meta name="robots" content="noodp" />
      </Head>
      <style jsx global>{`
        #__next {
          height: 100%;
        }
      `}</style>
      <span className="cover-bg" />
      <Header
        modalOpened={mobileMenuOpened}
        setModalOpened={setMobileMenuOpened}
      />
      <main className="main">
          {children}
      </main>
      <Footer />
      <Modal
        open={mobileMenuOpened}
        setOpen={setMobileMenuOpened}
      />
    </>
  );
}
