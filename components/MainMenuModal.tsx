import Link from "./Link";

export default function MainMenuModal({
  open,
  setOpen,
}: IModalOpen) {
  return (
    <div className={`modal ${open ? "in" : ""}`} id="main-menu">
      <div className="wrap">
        <div className="main-menu">
          <div className="main-menu__search">
            <input
              type="text"
              className="main-menu__search-input"
              placeholder="Поиск по сайту"
            />
          </div>
          <div className="main-menu__holder">
            <div className="main-menu__item">
              <div className="main-menu__objects">
                <Link to={"/"} className="main-menu__object">
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title">
                    Все услуги и&nbsp;сервисы
                  </p>
                </Link>
                <Link to="/" className="main-menu__object">
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title">
                    База полезных знаний
                  </p>
                </Link>
                <Link
                  to="/"
                  className="main-menu__object main-menu__object_large"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title">
                    Цифровая
                    <br />
                    система Онлайн&nbsp;Патент
                  </p>
                </Link>
              </div>
              <div className="main-menu__objects">
                <Link
                  to="/"
                  className="main-menu__object main-menu__object_small"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title main-menu__object-title_small">
                    Бесплатная проверка
                  </p>
                  <span className="main-menu__object-text">
                    Товарного знака
                  </span>
                </Link>
                <Link
                  to="/"
                  className="main-menu__object main-menu__object_small"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title main-menu__object-title_small">
                    Бесплатный поиск
                  </p>
                  <span className="main-menu__object-text">
                    Полезных моделей и&nbsp;изобретений
                  </span>
                </Link>
                <Link
                  to="/"
                  className="main-menu__object  main-menu__object_small"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title main-menu__object-title_small">
                    Бесплатный поиск
                  </p>
                  <span className="main-menu__object-text">
                    Свидетельств на&nbsp;программы
                  </span>
                </Link>
              </div>
              <div className="main-menu__objects">
                <Link
                  to="/"
                  className="main-menu__object main-menu__object_medium"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title">
                    Регистрация
                    <br />
                    товарного
                    <br />
                    знака
                  </p>
                  <span className="main-menu__object-icon" />
                </Link>
                <Link
                  to="/"
                  className="main-menu__object main-menu__object_medium"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title">
                    Внесение в&nbsp;Реестр&nbsp;ПО
                  </p>
                </Link>
                <Link
                  to="/"
                  className="main-menu__object  main-menu__object_medium"
                >
                  <span className="main-menu__object-arrow" />
                  <p className="main-menu__object-title">
                    Регистрация программы
                  </p>
                </Link>
              </div>
            </div>
            <div className="main-menu__item">
              <div className="main-menu__holder-menu">
                <div className="main-menu__block-menu">
                  <span className="main-menu__title">Защитить сейчас</span>
                  <Link to="/" className="main-menu__link-menu">
                    Товарный знак
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Программа для&nbsp;ЭВМ
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Полезная модель
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Изобретение
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Дизайн изделия
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    База данных
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    SMM-Броня
                  </Link>
                </div>
                <div className="main-menu__block-menu">
                  <span className="main-menu__title">Узнать больше</span>
                  <Link to="/" className="main-menu__link-menu">
                    База знаний
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Поиск по МКТУ
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Вебинары
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Тесты и игры
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Блог и&nbsp;новости
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Наши партнеры
                  </Link>
                  <Link to="/" className="main-menu__link-menu">
                    Контакты
                  </Link>
                </div>
              </div>
              <div className="main-menu__holder-menu">
                <div className="main-menu__block-menu main-menu__block-menu_bottom">
                  <div className="main-menu__contacts">
                    <Link to="tel:88003333662" className="main-menu__phone">
                      8 800 333-36-62
                    </Link>
                    <Link
                      to="mailto:support@onlinepatent.ru"
                      className="main-menu__contacts-mail"
                    >
                      support@onlinepatent.ru
                    </Link>
                  </div>
                </div>
                <div className="main-menu__block-menu main-menu__block-menu_bottom">
                  <div className="main-menu__btn-holder">
                    <div className="main-menu__btn">
                      <div className="btn-ps btn-ps_orange contact-us-popup">
                        <span className="btn-ps__arrow" />
                        <span className="btn-ps__text">Заказать звонок</span>
                      </div>
                    </div>
                  </div>
                  <div className="main-menu__lang">
                    <span className="lang-switch__text">Версия сайта</span>
                    <Link to="/" className="lang-switch">
                      <span className="lang-switch__item is-active">Ru</span>
                      <span className="lang-switch__item">En</span>
                      <span className="lang-switch__roller" />
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="Mobile-menu">
          <div className="Mobile-menu__select">
            <div className="Mobile-menu__select-item" data-target=".view-1">
              Меню
            </div>
            <div
              className="Mobile-menu__select-item is-active"
              data-target=".view-2"
            >
              Услуги
            </div>
          </div>
          <div className="Mobile-menu__tabs">
            <div className="Mobile-menu__tab view-1">
              <div className="Mobile-menu__services">
                <Link to="/" className="Mobile-menu__services-item-link">
                  О&nbsp;компании
                </Link>
                <Link to="/" className="Mobile-menu__services-item-link">
                  Отзывы о&nbsp;нас
                </Link>
                <Link to="/" className="Mobile-menu__services-item-link">
                  Наши партнеры
                </Link>
                <Link to="/" className="Mobile-menu__services-item-link">
                  Блог и&nbsp;новости
                </Link>
                <Link to="/" className="Mobile-menu__services-item-link">
                  База знаний
                </Link>
                <Link to="/" className="Mobile-menu__services-item-link">
                  Контакты
                </Link>
              </div>
            </div>
            <div className="Mobile-menu__tab view-2 is-active">
              <div className="Mobile-menu__services">
                <Link to="/" className="Mobile-menu__services-item-link">
                  Все услуги и&nbsp;цены
                </Link>
                <div className="Mobile-menu__services-item">
                  <span className="Mobile-menu__services-item-title">
                    Проверка по&nbsp;базам Роспатента
                  </span>
                  <div className="Mobile-menu__services-inside">
                    <div className="Mobile-menu__services-slider">
                      <div className="Mobile-menu__services-holder">
                        <Link to="/" className="Mobile-menu__services-link">
                          <p className="Mobile-menu__services-name">
                            Бесплатная проверка товарного знака
                          </p>
                        </Link>
                        <Link to="/" className="Mobile-menu__services-link">
                          <p className="Mobile-menu__services-name">
                            Бесплатная проверка патента на&nbsp;изобретение
                          </p>
                        </Link>
                        <Link to="/" className="Mobile-menu__services-link">
                          <p className="Mobile-menu__services-name">
                            Бесплатная проверка программы для&nbsp;ЭВМ
                          </p>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Mobile-menu__services-item">
                  <span className="Mobile-menu__services-item-title">
                    Тип объекта
                  </span>
                  <div className="Mobile-menu__services-inside">
                    <div className="Mobile-menu__services-slider">
                      <div className="Mobile-menu__services-holder">
                        <Link className="Mobile-menu__services-link" to="/">
                          <p className="Mobile-menu__services-name">
                            Услуга по типу объекта
                          </p>
                          <p className="Mobile-menu__services-price">
                            от 2000&nbsp;₽
                          </p>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="Mobile-menu__bottom">
              <form className="Mobile-menu__search">
                <div className="main-menu__search">
                  <input
                    type="text"
                    className="main-menu__search-input"
                    placeholder="поиск по сайту"
                  />
                </div>
              </form>
              <div className="main-menu__lang">
                <span>Версия сайта</span>
                <Link to="/" className="lang-switch">
                  <span className="lang-switch__item {if $LANG eq 'ru'}is-active{/if}">
                    Ru
                  </span>
                  <span className="lang-switch__item {if $LANG eq 'en'}is-active{/if}">
                    En
                  </span>
                  <span className="lang-switch__roller" />
                </Link>
              </div>
              <div className="main-menu__contacts">
                <Link to="tel:88003333662" className="main-menu__phone">
                  8 800 333-36-62
                </Link>
                <div className="main-menu__addresses">
                  <div className="main-menu__contacts-text">
                    Москва, Бережковская наб., д.&nbsp;6
                  </div>
                  <Link
                    to="mailto:support@onlinepatent.ru"
                    className="main-menu__contacts-link"
                  >
                    support@onlinepatent.ru
                  </Link>
                </div>
              </div>
              <div className="social">
                <Link
                  to="/"
                  rel="nofollow"
                  target="_blank"
                  className="social__item social__item_insta"
                />
                <Link
                  to="/"
                  rel="nofollow"
                  target="_blank"
                  className="social__item social__item_vk"
                />
                <Link
                  to="/"
                  rel="nofollow"
                  target="_blank"
                  className="social__item social__item_twit"
                />
                <Link
                  to="/"
                  rel="nofollow"
                  target="_blank"
                  className="social__item social__item_youtube"
                />
                <Link
                  to="/"
                  rel="nofollow"
                  target="_blank"
                  className="social__item social__item_fb"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
