import EditorJS from "@editorjs/editorjs";
import React, { useState, useRef } from "react";
import { createReactEditorJS } from "react-editor-js";
import { editArticle } from "../api";
import { observer } from "mobx-react-lite";

import { EDITOR_JS_TOOLS } from "../utils/editor.js/react-editor-constants";
import Modal from "./Modal";

const ReactEditorJS = createReactEditorJS();

interface IModalProps extends IModalOpen {
  data: ArticleDetail;
  setData: (data: ArticleDetail) => void;
}

function ModalEditor({ open, setOpen, data, setData }: IModalProps) {
  const editorJS = useRef(null);
  const editorTitleRef = useRef<HTMLTextAreaElement>(null);

  const [loading, setLoading] = useState(false);
  const [title, setTitle] = useState(data.article_name);

  const handleInitialize = React.useCallback((instance) => {
    editorJS.current = instance;
  }, []);

  const handleTitleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    event.preventDefault();
    setTitle(event.target.value);
    if (editorTitleRef.current) {
      editorTitleRef.current.style.height = "auto";
      editorTitleRef.current.style.height = `${editorTitleRef.current.scrollHeight}px`;
    }
  };

  const handleSave = React.useCallback(async () => {
    if (editorJS.current) {
      setLoading(true);
      const savedData = await (editorJS.current as EditorJS).save();

      const newData = {
        ...data,
        article_name: title,
        content: savedData as DataProp,
      };
      await editArticle(newData);
      setData(newData);
      setOpen(false);
      setLoading(false);
    }
  }, [title, data, setData, setOpen]);

  const submit = (event: React.MouseEvent<HTMLElement>) => {
    handleSave();
  };

  return (
    <Modal open={open} setOpen={setOpen}>
      <>
        <textarea
          className="editor-title"
          placeholder="Новый заголовок"
          ref={editorTitleRef}
          onChange={handleTitleChange}
        >
          {title}
        </textarea>
        <ReactEditorJS
          onInitialize={handleInitialize}
          tools={EDITOR_JS_TOOLS}
          defaultValue={data?.content}
        />
        <button
          className="btn btn-full size-l btn-shadow"
          onClick={(event) => submit(event)}
        >
          {loading ? "loading" : "Сохранить"}
        </button>
      </>
    </Modal>
  );
}

export default observer(ModalEditor);
