import NextLink from "next/link";

export default function Link({
  to,
  className,
  children,
  as,
  ...rest
}: {
  to: string;
  children?: any;
  className?: string;
  as?: string;
  target?: string;
  rel?: string;
}) {
  return (
    <NextLink href={to} as={as}>
      <a className={className} {...rest}>
        {children}
      </a>
    </NextLink>
  );
}
