import Link from "./Link";
import moment from "moment";
import { declinations, opImageUrl } from "../utils";
import HashtagLink from "./HashtagLink";

type CardProps = ArticleResponse & {
  type?: "onTop" | "onBottom" | undefined;
};

const Card = ({
  article_name: title,
  article_id: id,
  tag_name: tag,
  tagslug,
  date,
  time_to_read: timeToRead,
  image_thumbail_url: imageUrl,
  type,
}: CardProps) => {

  const BaseCard = (
    <div className="card">
      <div className="card__media">
        <span
          className="card__media-pic"
          style={{ backgroundImage: `url(${opImageUrl(imageUrl)})`}}
        />
      </div>
      <div className="card__body">
        <Link to={`/article/${id}`} className="card__title">
          {title}
        </Link>
        <div className="article-data card__data">
          <div className="article-data__item">
            <HashtagLink to={tagslug}>{tag}</HashtagLink>
          </div>
          <div className="article-data__item">
            <time className="article-date">
              <span className="article-date__desktop">
                {moment(date).format("DD MMMM YYYY")}
              </span>
              <span className="article-date__mobile">
                {moment(date).format("DD.MM.YY")}
              </span>
            </time>
          </div>
          <div className="article-data__item">
            <time className="article-time black">
              <span className="icon icon-time article-time__icon" />
              <em>{timeToRead}</em>
            </time>
          </div>
        </div>
      </div>
    </div>
  );

  const TopCard = (
    <div  className="card card-full">
      <div className="card__media">
       <span
        className="card__media-pic"
        style={{ backgroundImage: `url(${opImageUrl(imageUrl)})`}}
       />
      </div>
      <div className="card__body">
        <Link className="card__title" to={`/article/${id}`}><h3 className="block-title">{title}</h3></Link>
        <div className="article-data card__data">
          <div className="article-data__item">
            <HashtagLink to={tagslug}>{tag}</HashtagLink>
          </div>
          <div className="article-data__item">
            <time className="article-date">
              <span className="article-date__desktop">
                {moment(date).format("DD MMMM YYYY")}
              </span>
              <span className="article-date__mobile">
                {moment(date).format("DD.MM.YY")}
              </span>
            </time>
          </div>
          <div className="article-data__item">
            <time className="article-time black">
              <span className="icon icon-time article-time__icon" />
              <em>{timeToRead}</em>
            </time>
          </div>
        </div>
      </div>
    </div>
  );

  // const BottomCard = (
  //   <section className="journal-news-section">
  //     <div className="wrap">
  //       <article className="journal-news-single">
  //         <div className="journal-news-single__item">
  //           <div className="journal-news-single__media">
  //             <span
  //               className="journal-news-single__pic"
  //               style={{ backgroundImage: `url(${opImageUrl(imageUrl)})`}}
  //             />
  //           </div>
  //         </div>
  //         <div className="journal-news-single__item">
  //           <div className="journal-news-single__data">
  //             <div className="journal-story-data journal-story-data_shadow">
  //               <div className="journal-story-data__item-shadow">
  //                 <span className="journal__hashtag journal__hashtag_transparent journal__hashtag_invert-mobile">
  //                   #{tag}
  //                 </span>
  //               </div>
  //               <div className="journal-story-data__item-shadow">
  //                 <time className="journal__date">
  //                   <span className="journal__date-desktop">
  //                     {moment(date).format("DD MMMM YYYY")}
  //                   </span>
  //                   <span className="journal__date-mobile">
  //                     {moment(date).format("DD.MM.YY")}
  //                   </span>
  //                 </time>
  //               </div>
  //               <div className="journal-story-data__item-shadow">
  //                 <time className="journal__time">
  //                   <span className="journal__time-icon" />
  //                   {timeToRead}
  //                 </time>
  //               </div>
  //             </div>
  //           </div>
  //           <Link to={`/article/${id}`} className="journal-news-single__title">
  //             {title}
  //           </Link>
  //           <span className="journal__btn-read desktop-only">Прочесть</span>
  //         </div>
  //       </article>
  //     </div>
  //   </section>
  // );

  switch (type) {
    case "onBottom":
      // return BottomCard;
    case "onTop":
      return TopCard;
    default:
      return BaseCard;
  }
};

export default Card;
