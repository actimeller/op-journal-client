import Link from "./Link";
import React from "react";

type HashtagLink = {
  to: string;
  children: string;
  className?: string
};

export default function HashtagLink({ to, children, className }: HashtagLink) {
  return (
    <Link to={`/tag/${to}`} className={`hashtag ${className}`}>
      #{children}
    </Link>
  );
}
