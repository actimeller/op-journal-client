import _ from "lodash";
import moment from "moment";
import Link from "./Link";
import HashtagLink from "./HashtagLink";

export default function LatestNews({ cards }: { cards?: ArticleResponse[] }) {
  return (
    <div className="latest-news">
        <div className="title-row-btn main-page__title-block">
            <strong className="section-title title-row-btn__title">
                Последние новости
            </strong>
            <Link
                to="/tag/novosti"
                className="btn btn-ghost ghost size-s title-row-btn__btn"
            >
                <i className="btn__text"><span className="mobile-hidden">Показать </span>еще</i>
            </Link>
        </div>
        <div className="cards-set">
            {cards
                ?.filter((card, index) => card.tag_name === "Новости")
                .slice(0,10)
                .map(({ date, article_name: title, tag_name: tag, article_id: id, tagslug }) => (
                    <div className="cards-set__item latest-news__card" key={_.uniqueId()}>
                        <div className="card card-row">
                            <div className="card__media">
                                <span className="card__media-pic" style={{background:'black'}} />
                            </div>
                            <div className="card__body">
                                <Link to={`/article/${id}`} className="card__title">{title}</Link>
                                <div className="article-data card__data">
                                    <div className="article-data__item">
                                        <HashtagLink to={tagslug}>{tag}</HashtagLink>
                                    </div>
                                    <div className="article-data__item">
                                        <time className="article-date">
                                      <span className="article-date__desktop">
                                        {moment(date).format("DD MMMM YYYY")}
                                      </span>
                                            <span className="article-date__mobile">
                                        {moment(date).format("DD.MM.YY")}
                                      </span>
                                        </time>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
        </div>
    </div>
  );
}
