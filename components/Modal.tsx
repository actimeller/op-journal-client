import React, { useEffect, useState } from "react";
import { ReactElement } from "react";
import { CSSTransition } from "react-transition-group";

type IModal = IModalOpen & {
  children: ReactElement;
};


export default function Modal({ children, open, setOpen, ...props }: IModal) {
//   const [stateOpen, setStateOpen] = useState(false);

  const childrenWithProps = React.cloneElement(children, {
    ...props,
  });
  useEffect(() => {
    const handler = (event: KeyboardEvent) => {
      if (event.key === "Escape") {
        setOpen(false);
      }
    };
    document.addEventListener("keydown", handler);
    return () => {
      document.removeEventListener("keydown", handler);
    };
  }, [setOpen]);

//   useState(() => {
//     setStateOpen(true);
//   }, []);

  return (
    <CSSTransition
      in={open}
      timeout={300}
      classNames="Modal-transition"
      unmountOnExit
    //   onEnter={() => setShowButton(false)}
      // onExited={() => setShowButton(true)}
    >
      <div className="Modal">
        <div className="Modal-cover" onClick={() => setOpen(false)}></div>
        <div className="Modal-body">
          <button className="Modal-close" onClick={() => setOpen(false)}>
            +
          </button>
          {childrenWithProps}
        </div>
      </div>
    </CSSTransition>
  );
}
