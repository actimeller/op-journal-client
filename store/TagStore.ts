import { makeAutoObservable } from "mobx";
import { getTags } from "../api";

class TagStore {
  data: TagResponse[] = [];
  status: string = "loading";
  error: any;

  constructor() {
    makeAutoObservable(this);
    this.fetchTags();
  }

  fetchTags = async () => {
    try {
      this.error = "";
      this.status = "loading";
      const tagsResponse = await getTags();
      this.data = tagsResponse || [];
    } catch (error) {
      this.status = "error";
      this.data = [];
      this.error = error;
    }
  };
}

export default new TagStore();
