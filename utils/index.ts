// @@ usage declinations(21, ['секунда', 'секунды', 'секунд']))
export const declinations = (number: number, titles: string[]): string => {
  const cases = [2, 0, 1, 1, 1, 2];
  return `${number} ${
    titles[
      number % 100 > 4 && number % 100 < 20
        ? 2
        : cases[number % 10 < 5 ? number % 10 : 5]
    ]
  }`;
};

export const initials = (name: string): string =>
  name
    .split(" ")
    .map((symbol) => symbol[0].toUpperCase())
    .join("");

export const opImageUrl = (url: string) =>
  `${process.env.NEXT_PUBLIC_STATIC_FILES_URL}${url}`;
