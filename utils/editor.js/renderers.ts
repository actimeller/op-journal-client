import QuoteRenderer from "./tools/Quote/renderer";
import IncutRenderer from "./tools/Incut/renderer";
import VoteRenderer from "./tools/Vote/renderer";
import NumericRenderer from "./tools/Numeric/renderer";
import FaqRenderer from "./tools/Faq/renderer";
import ServiceRenderer from "./tools/Service/renderer";

const renderers = {
  quote: QuoteRenderer,
  incut: IncutRenderer,
  service: ServiceRenderer,
  vote: VoteRenderer,
  numeric: NumericRenderer,
  faq: FaqRenderer,
};

export default renderers;
