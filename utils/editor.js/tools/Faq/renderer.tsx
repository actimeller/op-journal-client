import { useState } from "react";

export default function FaqRenderer({
  data,
  className,
}: {
  data: {
    title?: string;
    list?: { question: string; answer: string }[];
  };
  className?: string;
}) {
  const [openKey, setOpenKey] = useState<string | null>(null);
  const { title, list } = data;
  const handleAnswer = (key: string) => {
    setOpenKey(key === openKey ? null : key);
  };

  return (
    <article className="widget faq">
      <div className="widget__title-block">
          <small className="widget__subtitle">Вопросы и ответы:</small>
          <strong className="widget__title">{title}</strong>
      </div>
      <div className="widget__body faq__body">
        {list?.map(({ question, answer }) => (
          <div className={`faq__block ${openKey === question && "is-open"}`} key={question}>
            <div
              className="faq__question"
              onClick={() => handleAnswer(question)}
            >
              <p className="faq__question-text">{question}</p>
                <span className="icon icon-plus-black faq__question-plus" />
            </div>
            <div className="faq__answer">
              <p className="faq__answer-text">{answer}</p>
            </div>
          </div>
        ))}
      </div>
    </article>
  );
}
