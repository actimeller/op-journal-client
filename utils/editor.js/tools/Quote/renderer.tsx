import Image from "next/image";

export default function QuoteRenderer({
  data,
  className,
}: {
  data: {
    text?: string;
    author?: string;
    role?: string;
    imageUrl?: string;
  };
  className?: string;
}) {
  const { text, author, role, imageUrl } = data;

    return (
        <article className="widget blockquote">
            <div className="widget__body blockquote__body">
                <blockquote>
                    <p className="blockquote__text">{text}</p>
                </blockquote>
                <figure>
                    {imageUrl && <Image src={imageUrl} className="blockquote__pic" alt={author} width={64} height={64} />}
                    <figcaption className="blockquote__figcaption">
                        {author && <p className="blockquote__figcaption-text">{author}</p>}
                        {role && <p className="blockquote__figcaption-text">{role}</p>}
                    </figcaption>
                </figure>
            </div>
        </article>
    );
}
