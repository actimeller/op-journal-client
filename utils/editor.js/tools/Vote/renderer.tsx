export default function VoteRenderer({
    data,
    className,
}: {
    data: {
        title?: string;
        firstAnswer?: string;
        secondAnswer?: string;
        thirdAnswer?: string;
    };
    className?: string;
}) {
    const { title, firstAnswer, secondAnswer, thirdAnswer } = data;

    return (
        <article className="widget vote">
            <div className="widget__title-block">
                <small className="widget__subtitle">Опрос сообщества:</small>
                <strong className="widget__title">{title}</strong>
            </div>
            <div className="widget__body vote__body">
                <div className="vote__buttons">
                    <div className="vote__btn-block">
                        <button type="button" className="vote__btn">
                            <span className="vote__btn-text">{firstAnswer}</span>
                        </button>
                    </div>
                    <div className="vote__btn-block">
                        <button type="button" className="vote__btn is-active">
                            <span className="vote__btn-text">{secondAnswer}</span>
                            <span className="vote__btn-text vote__btn-percent">38%</span>
                        </button>
                    </div>
                    <div className="vote__btn-block">
                        <button type="button" className="vote__btn">
                            <span className="vote__btn-text">{thirdAnswer}</span>
                        </button>
                    </div>
                </div>
            </div>
        </article>
    );
}
