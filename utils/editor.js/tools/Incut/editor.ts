type dataType = {
  text: string;
};
export default class Incut {
  api: any;
  readOnly: boolean;
  data: dataType;

  textPlaceholder: string;

  static DEFAULT_TEXT_PLACEHOLDER: string;

  static get toolbox() {
    return {
      icon:
        '<svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.65185 0.683111C4.70891 0.673477 4.76659 0.667946 4.82444 0.666561C4.89533 0.665872 4.96614 0.67141 5.03605 0.683111H9.07903C9.23955 0.683127 9.39349 0.746901 9.507 0.860407C9.6205 0.973913 9.68428 1.12785 9.68429 1.28838V2.4989C9.68543 2.57911 9.67061 2.65874 9.6407 2.73317C9.61079 2.8076 9.56639 2.87534 9.51007 2.93246C9.45376 2.98958 9.38665 3.03493 9.31265 3.06589C9.23865 3.09685 9.15924 3.11279 9.07903 3.11279C8.99882 3.11279 8.9194 3.09685 8.8454 3.06589C8.77141 3.03493 8.7043 2.98958 8.64798 2.93246C8.59167 2.87534 8.54726 2.8076 8.51736 2.73317C8.48745 2.65874 8.47263 2.57911 8.47376 2.4989V1.89364H5.38604V10.3673H6.65797C6.73817 10.3662 6.8178 10.381 6.89223 10.4109C6.96666 10.4408 7.0344 10.4852 7.09152 10.5416C7.14864 10.5979 7.194 10.665 7.22496 10.739C7.25591 10.813 7.27185 10.8924 7.27185 10.9726C7.27185 11.0528 7.25591 11.1322 7.22496 11.2062C7.194 11.2802 7.14864 11.3473 7.09152 11.4037C7.0344 11.46 6.96666 11.5044 6.89223 11.5343C6.8178 11.5642 6.73817 11.579 6.65797 11.5779H5.03841C4.90846 11.5992 4.77589 11.5992 4.64594 11.5779H3.02638C2.94618 11.579 2.86655 11.5642 2.79212 11.5343C2.71769 11.5044 2.64995 11.46 2.59283 11.4037C2.53571 11.3473 2.49036 11.2802 2.4594 11.2062C2.42844 11.1322 2.4125 11.0528 2.4125 10.9726C2.4125 10.8924 2.42844 10.813 2.4594 10.739C2.49036 10.665 2.53571 10.5979 2.59283 10.5416C2.64995 10.4852 2.71769 10.4408 2.79212 10.4109C2.86655 10.381 2.94618 10.3662 3.02638 10.3673H4.29831V1.89364H1.21059V2.4989C1.21172 2.57911 1.19691 2.65874 1.167 2.73317C1.13709 2.8076 1.09269 2.87534 1.03637 2.93246C0.980054 2.98958 0.912947 3.03493 0.838949 3.06589C0.764951 3.09685 0.685538 3.11279 0.605325 3.11279C0.525112 3.11279 0.445699 3.09685 0.371701 3.06589C0.297704 3.03493 0.230596 2.98958 0.17428 2.93246C0.117963 2.87534 0.0735607 2.8076 0.0436523 2.73317C0.0137439 2.65874 -0.00107376 2.57911 6.05309e-05 2.4989V1.28838C7.65806e-05 1.12785 0.0638505 0.973913 0.177356 0.860407C0.290862 0.746901 0.444804 0.683127 0.605325 0.683111H4.65185Z" fill="black"/><path d="M8.94971 6.764C9.10398 6.50141 9.38626 6.34385 9.68824 6.34714H15.1482C15.4502 6.34385 15.7325 6.50141 15.8868 6.764C16.0377 7.02659 16.0377 7.34827 15.8868 7.61086C15.7325 7.87345 15.4502 8.031 15.1482 8.02772H9.68824C9.38626 8.031 9.10398 7.87345 8.94971 7.61086C8.79871 7.34827 8.79871 7.02659 8.94971 6.764Z" fill="black"/><path d="M8.94971 10.1252C9.10398 9.86257 9.38626 9.70502 9.68824 9.7083H12.6235C12.9255 9.70502 13.2078 9.86257 13.3621 10.1252C13.513 10.3878 13.513 10.7094 13.3621 10.972C13.2078 11.2346 12.9255 11.3922 12.6235 11.3889H9.68824C9.38626 11.3922 9.10398 11.2346 8.94971 10.972C8.79871 10.7094 8.79871 10.3878 8.94971 10.1252Z" fill="black"/></svg>',
      title: "Incut",
    };
  }

  static get enableLineBreaks() {
    return true;
  }

  get CSS(): Record<string, string> {
    return {
      baseClass: this.api.styles.block,
      wrapper: "cdx-incut",
      text: "cdx-incut__text",
      input: this.api.styles.input,
      settingsWrapper: "cdx-incut-settings",
      settingsButton: this.api.styles.settingsButton,
      settingsButtonActive: this.api.styles.settingsButtonActive,
    };
  }

  constructor({ data, config, api, readonly }: IEditor<dataType>) {
    this.api = api;
    this.readOnly = readonly;

    this.textPlaceholder =
      config.textPlaceholder || Incut.DEFAULT_TEXT_PLACEHOLDER;

    this.data = {
      text: data.text || "",
    };
  }

  render() {
    const container = this._make("div", [this.CSS.baseClass, this.CSS.wrapper]);
    const text = this._make("div", [this.CSS.input, this.CSS.text], {
      contentEditable: !this.readOnly,
      innerHTML: this.data.text,
    });

    text.dataset.placeholder = this.textPlaceholder;

    container.appendChild(text);

    return container;
  }

  save(incutElement: HTMLDivElement) {
    const text = incutElement.querySelector(`.${this.CSS.text}`);

    return Object.assign(this.data, {
      text: text?.innerHTML,
    });
  }

  static get sanitize() {
    return {
      text: {
        br: true,
      },
      alignment: {},
    };
  }

  _make(
    tagName: string,
    classNames: string[] = [],
    attributes: Record<string, any> = {}
  ) {
    const el = document.createElement(tagName);

    if (Array.isArray(classNames)) {
      el.classList.add(...classNames);
    } else if (classNames) {
      el.classList.add(classNames);
    }

    for (const attrName in attributes) {
      (el as any)[attrName] = attributes[attrName];
    }

    return el;
  }
}
