export default function IncutRenderer({
    data,
    className,
}: {
    data: {
        text?: string;
    };
    className?: string;
}) {
    const { text } = data;

    return (
        <article className="widget incut">
            <div className="widget__body incut__body">
                <p className="incut__text">{text}</p>
            </div>
        </article>
    );
}
