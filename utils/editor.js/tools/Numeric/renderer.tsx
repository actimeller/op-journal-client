export default function NumericRenderer({
   data,
   className,
}: {
    data: {
        numeral?: string;
        sign?: string;
    };
    className?: string;
}) {
    const { numeral, sign } = data;

    return (
        <article className="widget numeric">
            <div className="widget__body numeric__body">
                <div className="numeric__numeral-block">
                    <strong className="numeric__numeral">{numeral}</strong>
                </div>
                <p className="numeric__sign">{sign}</p>
            </div>
        </article>
    );
}
