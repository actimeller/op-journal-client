export default function ServiceRenderer({
    data,
    className,
}: {
    data: {
        title?: string;
        prepearing?: string;
        expertise?: string;
        validity?: string;
        sum?: string;
        button?: string;
        link?: string;
    };
    className?: string;
}) {
    const { title, prepearing, expertise, validity, sum, link } = data;

    return (
        <article className="widget service">
            <div className="widget__title-block">
                <small className="widget__subtitle">Подходящая услуга:</small>
            </div>
            <div className="widget__body service__body">
                <strong className="service__title">{title}</strong>
                <div className="service__row">
                    <p className="service__row-item">Подготовка <span className="service__prepearing">{prepearing}</span></p>
                    <p className="service__row-item">Экспертиза <span className="service__expertise">{expertise}</span></p>
                    <p className="service__row-item">Срок действия <span className="service__validity">{validity}</span></p>
                </div>
                <div className="service__row-bottom">
                    <strong className="service__sum">{sum}&nbsp;₽</strong>
                    <a href={link} className="button-rectangle service__button">
                        <span className="button-text">Зарегистрировать сейчас</span>
                        <em className="button-icon" />
                    </a>
                </div>
            </div>
        </article>
    );
}
