declare module "@editorjs/embed";
declare module "@editorjs/table";
declare module "@editorjs/list";
declare module "@editorjs/warning";
declare module "@editorjs/code";
declare module "@editorjs/link";
declare module "@editorjs/image";
declare module "@editorjs/raw";
declare module "@editorjs/header";
declare module "@editorjs/marker";
declare module "@editorjs/checklist";
declare module "@editorjs/delimiter";
declare module "@editorjs/inline-code";
declare module "@editorjs/simple-image";
declare module "editorjs-header-with-anchor";

type IEditor<D> = {
  data: D;
  config: any;
  api: any;
  readonly: boolean
}
