import Embed from "@editorjs/embed"; // https://github.com/editor-js/embed
import Table from "@editorjs/table"; // https://github.com/editor-js/table
import List from "@editorjs/list"; // https://github.com/editor-js/list
import Warning from "@editorjs/warning"; // https://github.com/editor-js/warning
import Code from "@editorjs/code"; // https://github.com/editor-js/code
import LinkTool from "@editorjs/link"; // https://github.com/editor-js/link
import ImageTool from "@editorjs/image"; // https://github.com/editor-js/image
import Raw from "@editorjs/raw"; // https://github.com/editor-js/raw
// import Header from "@editorjs/header"; // https://github.com/editor-js/header
import Header from "editorjs-header-with-anchor"; // https://github.com/Aleksst95/header-with-anchor
// import Quote from '@editorjs/quote'; // https://github.com/editor-js/quote
import Quote from "./tools/Quote/editor";
import Incut from "./tools/Incut/editor";
import Vote from "./tools/Vote/editor";
import Numeric from "./tools/Numeric/editor";
import Faq from "./tools/Faq/editor";
import Service from "./tools/Service/editor";
import Marker from "@editorjs/marker"; // https://github.com/editor-js/marker
import CheckList from "@editorjs/checklist"; // https://github.com/editor-js/checklist
import Delimiter from "@editorjs/delimiter"; // https://github.com/editor-js/Delimiter
import InlineCode from "@editorjs/inline-code"; // https://github.com/editor-js/Inline-Code
import SimpleImage from "@editorjs/simple-image"; // https://github.com/editor-js/simple-image

import { ENDPOINTS } from "../../api";

const { NEXT_PUBLIC_API_URL } = process.env;

export const EDITOR_JS_TOOLS = {

  embed: Embed,
  table: Table,
  marker: Marker,
  list: List,
  code: Code,
  header: {
    class: Header,
    config: {
      allowAnchor: true,
    }
  },
  linkTool: {
    class: LinkTool,
    config: {
      endpoint: NEXT_PUBLIC_API_URL + ENDPOINTS.EDITOR.FETCH_URL,
    },
  },
  image: {
    class: ImageTool,
    config: {
      endpoints: {
        byFile: NEXT_PUBLIC_API_URL + ENDPOINTS.EDITOR.IMAGE_UPLOAD,
      },
    },
  },
  quote: {
    class: Quote,
    inlineToolbar: true,
    shortcut: "CMD+SHIFT+O",
    config: {
      textPlaceholder: "Цитата*",
      authorPlaceholder: "Автор",
      rolePlaceholder: "Должность",
      imageUrlPlaceholder: "Url изображения (http...)",
    },
  },
  incut: {
    class: Incut,
    inlineToolbar: true,
    config: {
      textPlaceholder: "Врезка*",
    },
  },
  vote: {
    class: Vote,
    inlineToolbar: true,
    config: {
      titlePlaceholder: "Название опроса*",
      firstAnswerPlaceholder: "Первый вариант",
      secondAnswerPlaceholder: "Второй вариант",
      thirdAnswerPlaceholder: "Третий вариант",
    },
  },
  numeric: {
    class: Numeric,
    inlineToolbar: true,
    config: {
      numeralPlaceholder: "Цифра*",
      signPlaceholder: "Подпись",
    },
  },
  faq: {
    class: Faq,
    inlineToolbar: true,
    config: {
      titlePlaceholder: "Название темы*",
      questionPlaceholder: "Вопрос",
      answerPlaceholder: "Ответ",
    },
  },
  service: {
    class: Service,
    inlineToolbar: true,
    config: {
      titlePlaceholder: "Название услуги*",
      prepearingPlaceholder: "Время подготовки (1-2 мес.)*",
      expertisePlaceholder: "Время экспертизы (10-18 мес.)*",
      validityPlaceholder: "Время срока действия (10-20 лет)*",
      sumPlaceholder: "Стоимость услуги*",
      buttonPlaceholder: "Текст в кнопке(Зарегистрировать сейчас)*",
      linkPlaceholder: "Ссылка на услугу*",
    },
  },
  delimiter: Delimiter,
  inlineCode: InlineCode,
  simpleImage: SimpleImage,
};
