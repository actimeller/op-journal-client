import { useEffect, useState } from "react";
import { GetServerSideProps } from "next";
import dynamic from "next/dynamic";
import Blocks from "editorjs-blocks-react-renderer";
import moment from "moment";

import {
  getArticleById,
  getArticles,
  getAuthorById,
} from "../../api";

import MainContainer from "../../components/MainContainer";
import Recommended from "../../components/Recommended";
import HashtagLink from "../../components/HashtagLink";
import renderers from "../../utils/editor.js/renderers";
const ModalEditor = dynamic(() => import("../../components/ModalEditor"), {
  ssr: false,
});

interface ArticleProps {
  article: Nullable<ArticleDetail>;
  cards: Nullable<ArticleResponse[]>;
  author: Nullable<AuthorResponse>;
}

export default function Article({ article, cards, author }: ArticleProps) {
  const [modalEditorOpened, setModalEditorOpened] = useState(false);
  const [data, setData] = useState(article)

  useEffect(() => {
    document.body.classList[!modalEditorOpened ? "remove" : "add"](
      "padding-right-15",
      "modal-open"
    );
  }, [modalEditorOpened]);

  if (!data) return <>no data</>;

  const {
    article_name: title,
    tag_name: tag,
    tagslug,
    time_to_read: timeToRead,
    date,
    image_thumbail_url: imageUrl,
    content,
  } = data;

  return (
    <MainContainer>
      <section className="article-page-section">
        <div className="wrap">
          <div className="editor__page-holder">
            <button className="btn btn-shadow editor__toggle-btn" onClick={() => setModalEditorOpened(true)}>
              <span className="btn__text">Редактировать статью</span>
            </button>
          </div>
          <div className="main-columns article-page">
            <div className="main-columns__aside aside">
              <aside className="aside__block">
                <h3 className="block-title aside__title">Оглавление</h3>
                <div className="aside-bg">
                  <ul className="aside-menu article-page__aside-menu">
                    <li className="aside-menu__item">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-op-black aside-menu__icon" />
                        <strong className="aside-menu__item-name">Заголовок</strong>
                      </a>
                    </li>
                    <li className="aside-menu__item">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-bookmark aside-menu__icon" />
                        <strong className="aside-menu__item-name">Заголовок h2</strong>
                      </a>
                    </li>
                    <li className="aside-menu__item">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-bookmark aside-menu__icon" />
                        <strong className="aside-menu__item-name">Лучше технологии - качественнее сталь</strong>
                      </a>
                    </li>
                    <li className="aside-menu__item aside-menu__drop">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-bookmark aside-menu__icon" />
                        <strong className="aside-menu__item-name">Заголовок h2</strong>
                      </a>
                      <ul className="aside-menu__drop-list">
                        <li className="aside-menu__item aside-menu__link_drop">
                          <a href="#" className="aside-menu__link">
                            <strong className="aside-menu__item-name">Подзаголовок</strong>
                          </a>
                        </li>
                        <li className="aside-menu__item aside-menu__link_drop">
                          <a href="#" className="aside-menu__link">
                            <strong className="aside-menu__item-name">Подзаголовок</strong>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="aside-menu__item">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-rocket aside-menu__icon" />
                        <strong className="aside-menu__item-name">Услуга</strong>
                      </a>
                    </li>
                    <li className="aside-menu__item">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-bookmark aside-menu__icon" />
                        <strong className="aside-menu__item-name">Опрос</strong>
                      </a>
                    </li>
                    <li className="aside-menu__item">
                      <a href="#" className="aside-menu__link">
                        <span className="icon icon-bookmark aside-menu__icon" />
                        <strong className="aside-menu__item-name">FAQ</strong>
                      </a>
                    </li>
                  </ul>
                  <div className="share">
                    <button className="btn btn-shadow btn-full">
                      <span className="icon icon-share btn__icon" />
                      <i className="btn__text btn__text_xl">Поделиться</i>
                    </button>
                  </div>
                </div>
              </aside>
              <aside className="aside__block">
                <h3 className="block-title aside__title">Поделиться</h3>
                <div className="aside__share">
                  <ul className="share-row">
                    <li className="share-row__item">
                      <button className="share-row__item-btn">
                        <em className="soc-icon soc-icon-Fb" />
                      </button>
                    </li>
                    <li className="share-row__item">
                      <button className="share-row__item-btn">
                        <em className="soc-icon soc-icon-Inst" />
                      </button>
                    </li>
                    <li className="share-row__item">
                      <button className="share-row__item-btn">
                        <em className="soc-icon soc-icon-Tw" />
                      </button>
                    </li>
                    <li className="share-row__item">
                      <button className="share-row__item-btn">
                        <em className="soc-icon soc-icon-Vk" />
                      </button>
                    </li>
                    <li className="share-row__item">
                      <button className="share-row__item-btn">
                        <em className="soc-icon soc-icon-Link" />
                      </button>
                    </li>
                  </ul>
                </div>
              </aside>
              <aside className="aside__block another-articles article-page__another-articles">
                <h3 className="block-title aside__title">Другие статьи</h3>
                <div className="card card-row another-articles__card">
                  <div className="card__media">
                    <span className="card__media-pic" style={{backgroundImage: 'url(\'https://www.tesla.com/tesla_theme/assets/img/_vehicle_redesign/roadster_and_semi/roadster/hero.jpg\')'}}/>
                  </div>
                  <div className="card__body">
                    <a className="card__title" href="#">
                      Товарные знаки для самозанятых и отказ суда запретить продажи смартфонов Samsung: главные новости в сфере ИС
                    </a>
                  </div>
                </div>
                <div className="card card-row another-articles__card">
                  <div className="card__body">
                    <a className="card__title" href="#">
                      Товарные знаки для самозанятых и отказ суда запретить продажи смартфонов Samsung: главные новости в сфере ИС
                    </a>
                  </div>
                </div>
                <div className="card card-row another-articles__card">
                  <div className="card__media">
                    <span className="card__media-pic" style={{backgroundImage: 'url(\'https://www.tesla.com/tesla_theme/assets/img/_vehicle_redesign/roadster_and_semi/roadster/hero.jpg\')'}}/>
                  </div>
                  <div className="card__body">
                    <a className="card__title" href="#">
                      Товарные знаки для самозанятых и отказ суда запретить продажи смартфонов Samsung: главные новости в сфере ИС
                    </a>
                  </div>
                </div>
              </aside>
            </div>
            <div className="main-columns__center">
              <article className="article">
                <div className="article-page__article-top">
                  <div className="article-data">
                    <div className="article-data__item">
                      <HashtagLink to={tagslug}>{tag}</HashtagLink>
                    </div>
                    <div className="article-data__item mobile-hidden">
                      <time className="article-date">
                        <span className="article-date__desktop">
                          {moment(date).format("DD MMMM YYYY")}
                        </span>
                      </time>
                    </div>
                    <div className="article-data__item mobile-hidden">
                      <time className="article-time">
                        <span className="icon icon-time article-time__icon" />
                        <em>{timeToRead}</em>
                      </time>
                    </div>
                  </div>
                  <div className="share mobile-hidden">
                    <button className="btn btn-shadow share__btn">
                      <span className="icon icon-share btn__icon" />
                      <i className="btn__text">Поделиться</i>
                    </button>

                    <div className="drop-down right-start share__drop-down is-open">
                      <ul className="drop-down__list share__list">
                        <li className="drop-down__item share__item">
                          <button className="drop-down__btn share__item-btn">
                            <em className="soc-icon soc-icon-Vk share__item-icon" />
                            <span className="drop-down__text share__item-text">VK</span>
                          </button>
                        </li>
                        <li className="drop-down__item share__item">
                          <button className="drop-down__btn share__item-btn">
                            <em className="soc-icon soc-icon-Fb share__item-icon" />
                            <span className="drop-down__text share__item-text">Facebook</span>
                          </button>
                        </li>
                        <li className="drop-down__item share__item">
                          <button className="drop-down__btn share__item-btn">
                            <em className="soc-icon soc-icon-Tw share__item-icon" />
                            <span className="drop-down__text share__item-text">Twitter</span>
                          </button>
                        </li>
                        <li className="drop-down__item share__item">
                          <button className="drop-down__btn share__item-btn">
                            <em className="soc-icon soc-icon-Inst share__item-icon" />
                            <span className="drop-down__text share__item-text">Instagram</span>
                          </button>
                        </li>
                        <li className="drop-down__item share__item">
                          <button className="drop-down__btn share__item-btn">
                            <em className="soc-icon soc-icon-Link share__item-icon" />
                            <span className="drop-down__text share__item-text">Скопировать</span>
                          </button>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                <h1 className="prime-title article-page__title">{title}</h1>
                <div className="share article-page__share-mobile">
                  <button className="btn btn-shadow share__btn">
                    <span className="icon icon-share btn__icon" />
                    <i className="btn__text">Поделиться</i>
                  </button>

                  <div className="drop-down right-start share__drop-down is-open">
                    <ul className="drop-down__list share__list">
                      <li className="drop-down__item share__item">
                        <button className="drop-down__btn share__item-btn">
                          <em className="soc-icon soc-icon-Vk share__item-icon" />
                          <span className="drop-down__text share__item-text">VK</span>
                        </button>
                      </li>
                      <li className="drop-down__item share__item">
                        <button className="drop-down__btn share__item-btn">
                          <em className="soc-icon soc-icon-Fb share__item-icon" />
                          <span className="drop-down__text share__item-text">Facebook</span>
                        </button>
                      </li>
                      <li className="drop-down__item share__item">
                        <button className="drop-down__btn share__item-btn">
                          <em className="soc-icon soc-icon-Tw share__item-icon" />
                          <span className="drop-down__text share__item-text">Twitter</span>
                        </button>
                      </li>
                      <li className="drop-down__item share__item">
                        <button className="drop-down__btn share__item-btn">
                          <em className="soc-icon soc-icon-Inst share__item-icon" />
                          <span className="drop-down__text share__item-text">Instagram</span>
                        </button>
                      </li>
                      <li className="drop-down__item share__item">
                        <button className="drop-down__btn share__item-btn">
                          <em className="soc-icon soc-icon-Link share__item-icon" />
                          <span className="drop-down__text share__item-text">Скопировать</span>
                        </button>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="author article-page__visitor">
                  {author && (
                    <div className="author__avatar-block">
                      <span className="author__avatar-initials">
                        {author.initials}
                      </span>
                    </div>
                  )}
                  <div className="author__data">
                    {author && (
                        <span className="author__name">
                          {author.name}
                        </span>
                    )}
                    <div className="article-page__article-data-mobile">
                      <div className="article-page__article-data-item">
                        <time className="article-date">
                          <span className="article-date__mobile">
                            {moment(date).format("DD.MM.YYYY")}
                          </span>
                        </time>
                      </div>
                      <time className="article-time article-page__article-data-item">
                        {timeToRead} чтения
                      </time>
                    </div>
                  </div>
                </div>
                <div className="admin article__body">
                  <figure>
                    {/* todo: fancybox */}
                    <span className="fancybox">
                      <span
                        style={{
                          backgroundImage: `url(${process.env.NEXT_PUBLIC_STATIC_FILES_URL}${imageUrl})`,
                        }}
                      />
                    </span>
                  </figure>
                  <div>
                    <Blocks
                      data={content}
                      renderers={renderers}
                    />
                  </div>
                </div>
                <div className="article-signature mobile-hidden">
                  <div className="article-signature__top">
                    <button className="btn btn-shadow size-s clap">
                      <em className="icon icon-clap clap__icon" />
                      <strong className="clap__count">18</strong>
                      <span className="clap__text">-&nbsp;Хлоп</span>
                    </button>
                    <div className="article-signature__soc-block">
                      <a className="article-signature__soc-link" href="#"><em className="soc-icon soc-icon-Fb" /></a>
                      <a className="article-signature__soc-link" href="#"><em className="soc-icon soc-icon-Inst" /></a>
                      <a className="article-signature__soc-link" href="#"><em className="soc-icon soc-icon-Tw" /></a>
                      <a className="article-signature__soc-link" href="#"><em className="soc-icon soc-icon-Vk" /></a>
                      <a className="article-signature__soc-link" href="#"><em className="soc-icon soc-icon-Link" /></a>
                    </div>
                  </div>
                  <div className="article-signature__bottom">
                    <HashtagLink to={tagslug}>{tag}</HashtagLink>
                    <span className="article-signature__bottom-text">21 апреля 2021 в 13:44</span>
                    <div className="article-signature__bottom-text">
                      <em className="icon icon-views article-signature__icon" />
                      <span className="article-signature__views-text">24K</span>
                    </div>
                    <div className="article-signature__bottom-text">
                      Теги: netflix, товарные знаки, новости
                    </div>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>
      {cards && <Recommended cards={cards.slice(0, 10)} />}

      <ModalEditor
        open={modalEditorOpened}
        setOpen={setModalEditorOpened}
        data={data}
        setData={setData}
      />
    </MainContainer>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const id = parseInt(params?.id as string);

  const [articleResponse, articlesResponse] = await Promise.all([
    getArticleById(id),
    getArticles(),
  ]);

  const author = articleResponse
    ? await getAuthorById(articleResponse.author_id)
    : null;

  return {
    props: { 
      article: articleResponse,
      author,
      cards: articlesResponse 
    },
  };
};
