/* eslint-disable @next/next/no-page-custom-font */
import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html>
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500;600&display=swap"
        />
        <link
          rel="preload"
          type="font/woff"
          href="/fonts/Circe_Regular.woff"
          as="font"
        />
        <link
          rel="preload"
          type="font/woff"
          href="/fonts/Circe_Bold.woff"
          as="font"
        />
        <link
          rel="preload"
          type="font/woff2"
          href="/fonts/Promo.woff2"
          as="font"
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
