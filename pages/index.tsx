import { GetServerSideProps } from "next";
import { observer } from "mobx-react-lite";

import _ from "lodash";
import { getArticles, getLatestNews, getMainArticles } from "../api";
import { useState } from "react";

import LatestNews from "../components/LatestNews";
import Card from "../components/Card";
import MainContainer from "../components/MainContainer";
import Link from "../components/Link";

const tabsValues = {
  important: "Важное",
  bestOfMonth: "Лучшее за месяц",
};

interface HomeProps {
  cards: ArticleResponse[] | null;
  cardsMain: ArticleResponse[] | null;
  latestNews: ArticleResponse[] | null;
}

export function Home({ cards, cardsMain, latestNews }: HomeProps) {
  const [cardTop, cardBottom] = cardsMain || [];
  const [currentTab, setCurrentTab] = useState(tabsValues.important);

  return (
    <MainContainer
      ogTitle={"Журнал Онлайн Патента"}
      ogUrl={"https://onlinepatent.ru/journal/"}
      ogImg={"https://onlinepatent.ru/content/img/bl_onpat.png"}
      ogDescription={""}
    >
      <div className="main-columns main-page">
        <div className="main-columns__center">
          <article className="main-page__major-section">
            <div className="wrap">
              {cardTop && <Card {...cardTop} type="onTop" />}
            </div>
          </article>

          <article className="main-page__latest-news">
            <div className="wrap">
              {latestNews && <LatestNews cards={latestNews} />}
            </div>
          </article>

          <article className="main-page__news">
            <div className="wrap">
              <ul className="tabs main-page__tabs">
                {Object.values(tabsValues).map((tab) => (
                    <li
                        className={`btn btn-ghost ghost size-xl tabs__item ${
                            currentTab === tab && "black"
                        }`}
                        key={tab}
                        onClick={() => setCurrentTab(tab)}
                        aria-hidden
                    >
                      <i className="btn__text">{tab}</i>
                    </li>
                ))}
              </ul>

              <div className="tabs-block tabs-view1 is-on">
                <div className="cards-set">
                  {cards
                      ?.filter((card) =>
                          currentTab === tabsValues.important
                              ? card // todo: card important
                              : false
                      )
                      .slice(0, 10)
                      .map((card) => (
                          <div
                              className="cards-set__item main-page__news-card"
                              key={_.uniqueId()}
                          >
                            <Card {...card} />
                          </div>
                      ))}
                </div>
              </div>
              <div className="tabs__block tabs-view2">
                <div className="cards-set">
                  {cards
                      ?.filter((card) =>
                          currentTab === tabsValues.important
                              ? card // todo: card important
                              : false
                      )
                      .slice(0, 10)
                      .map((card) => (
                          <div
                              className="cards-set__item main-page__news-card"
                              key={_.uniqueId()}
                          >
                            <Card {...card} />
                          </div>
                      ))}
                </div>
              </div>
            </div>
          </article>

          <article className="main-page__presentation">
            <div className="wrap">
              <article className="presentation">
                <div className="presentation__item">
                  <strong className="prime-title presentation__title">Патент — это утечка информации</strong>
                  <p className="presentation__text">
                    Может ли патент защитить ваши интересы? Конечно вы делитесь с обществом своими наработками публикуя информацию о патенте, но это взаимовыгодная сделка, в обмен на целых 20 лет получаете возможность зарабатывать на патенте так, как только вы захотите.
                  </p>
                  <a href="#" className="btn white size-xl presentation__btn">
                    <i className="btn__text">Читать, как патентовать правильно </i>
                  </a>
                </div>
                <div className="presentation__item">
                  <img className="presentation__pic" src="https://onlinepatent.ru/files/tU0tWXilENTwRdyxfHFhQWOx1LAMAA8rxSHhrHD2.jpg" alt=""/>
                </div>
              </article>
            </div>
          </article>

          <article className="main-page__news-slider dark-theme">
            <div className="wrap">
              <div className="title-row-btn main-page__news-slider-title">
                <strong className="section-title">Новости компаний</strong>
                <button className="btn btn-ghost ghost size-s title-row-btn__btn" >
                  <i className="btn__text">Показать еще</i></button>
              </div>
              <div className="cards-slider-large">
                <div className="cards-slider-large__item">
                  <div className="card card-large">
                    <div className="card__media">
                      <span className="card__media-pic" style={{background:'white'}} />
                    </div>
                    <div className="card__body">
                      <Link to={'/'} className="card__title">История Sharp: как механический карандаш повлиял на становление гиганта электроники</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>

          <article className="main-page__rubrics">
            <div className="wrap">
              <strong className="section-title main-page__rubrics-title">Наши рубрики</strong>
              <div className="main-page__rubrics-row">
                <div className="main-page__rubrics-btns">
                  <button className="btn btn-ghost black main-page__rubrics-btn">
                    <i className="btn__text">Главное</i>
                  </button>
                  <button className="btn btn-ghost ghost main-page__rubrics-btn">
                    <i className="btn__text">Аналитика</i>
                  </button>
                  <button className="btn btn-ghost ghost main-page__rubrics-btn">
                    <i className="btn__text">Новости</i>
                  </button>
                  <button className="btn btn-ghost ghost main-page__rubrics-btn">
                    <i className="btn__text">Суды и споры</i>
                  </button>
                  <button className="btn btn-ghost ghost main-page__rubrics-btn">
                    <i className="btn__text">Технологии</i>
                  </button>
                  <button className="btn btn-ghost ghost main-page__rubrics-btn">
                    <i className="btn__text">Товарные знаки</i>
                  </button>
                  <button className="btn btn-ghost ghost main-page__rubrics-btn">
                    <i className="btn__text">Фарма</i>
                  </button>
                </div>
                <div className="main-page__rubrics-filters">
                  <button className="btn btn-ghost ghost">По просмотрам</button>
                </div>
              </div>

              <div className="cards-set cards-quad main-page__rubrics-cards">
                {cards
                    ?.filter((card) =>
                        currentTab === tabsValues.important
                            ? card // todo: card important
                            : false
                    )
                    .slice(0, 8)
                    .map((card) => (
                        <div
                            className="cards-set__item main-page__rubrics-card"
                            key={_.uniqueId()}
                        >
                          <Card {...card} />
                        </div>
                    ))}
              </div>

              <div className="main-page__rubrics-btn-more"><button className="btn btn-ghost ghost">Показать еще</button></div>
            </div>
          </article>
        </div>
      </div>
      {/*{cardBottom && <Card {...cardBottom} type="onBottom"/>}*/}
    </MainContainer>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const [articlesResponse, mainArticlesResponse, latestNewsReponse] =
  await Promise.all([getArticles(), getMainArticles(), getLatestNews()]);
  return {
    props: {
      cards: articlesResponse,
      cardsMain: mainArticlesResponse,
      latestNews: latestNewsReponse,
    },
  };
};

export default observer(Home);
