import MainContainer from "../../components/MainContainer";

export default function Kit () {
    return (
        <MainContainer>
            <section className="kit-section">
                <div className="wrap">
                    <div className="kit">
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn blue">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn blue size-xl">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn blue size-l">
                                <i className="btn__text">Кнопка</i>
                                <span className="icon icon-arrow-btn btn__icon" />
                            </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn blue size-s">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn blue size-xs">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn orange">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xl orange">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-l orange">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-s orange">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xs orange">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn black">
                                    <span className="icon icon-arrow-btn-left btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xl black">
                                    <span className="icon icon-arrow-btn-left btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-l black">
                                    <span className="icon icon-arrow-btn-left btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-s black">
                                    <span className="icon icon-arrow-btn-left btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xs black">
                                    <span className="icon icon-arrow-btn-left btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn red">
                                    <span className="icon icon-plus btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xl red">
                                    <span className="icon icon-plus btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-l red">
                                    <span className="icon icon-plus btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-s red">
                                    <span className="icon icon-plus btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xs red">
                                    <span className="icon icon-plus btn__icon" />
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn green">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-plus-black btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xl green">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-plus-black btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-l green">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-plus-black btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-s green">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-plus-black btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xs green">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-plus-black btn__icon" />
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn blue btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xl orange btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-l black btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-s red btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn size-xs green btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                    <span className="icon icon-arrow-btn btn__icon" />
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost ghost">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-xl orange">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-l black">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-s red">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-xs green">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost ghost btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-xl orange btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-l black btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-s red btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-ghost size-xs green btn-scale">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <button className="btn btn-shadow">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-shadow size-xl">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-shadow size-l">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-shadow size-s">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                            <div className="kit__btn-item">
                                <button className="btn btn-shadow size-xs">
                                    <i className="btn__text">Кнопка</i>
                                </button>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <a href="#" className="link link-arrow">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-arrow blue">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-arrow size-xl orange">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-arrow size-l black">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-arrow size-s red">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-arrow size-xs green">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <a href="#" className="link link-blank">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-blank link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-blank blue">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-blank link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-blank size-xl orange">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-blank link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-blank size-l black">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-blank link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-blank size-s red">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-blank link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-blank size-xs green">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-blank link__icon" />
                                </a>
                            </div>
                        </div>
                        <div className="kit__btn-row">
                            <div className="kit__btn-item">
                                <a href="#" className="link link-appear">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-appear blue">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-appear size-xl orange">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-appear size-l black">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-appear size-s red">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                            <div className="kit__btn-item">
                                <a href="#" className="link link-appear size-xs green">
                                    <i className="link__text">Ссылка</i>
                                    <span className="icon icon-arrow-btn link__icon" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </MainContainer>
    )
}