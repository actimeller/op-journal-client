import "../styles/styles.scss";
import "moment/locale/ru";

import NextNProgress from "nextjs-progressbar";
import type { AppProps } from "next/app";
import { useEffect, useState } from "react";
import ProtectPassword from "../components/ProtectPassword";
import { OP_JOURNAL_CLIENT_LOGGED } from "../utils/constants";

function App({ Component, pageProps }: AppProps) {
  const [logged, setLogged] = useState<Boolean>();
  useEffect(() => {
    setLogged(!!window.localStorage.getItem(OP_JOURNAL_CLIENT_LOGGED));
  }, []);

  if (process.env.PASSWORD_PROTECT && typeof logged === "undefined") {
    return null;
  } else if (process.env.PASSWORD_PROTECT && !logged) {
    return <ProtectPassword />
  } else {
    return (
      <>
        <NextNProgress />
        <Component {...pageProps} />
      </>
    );
  }
}

export default App;
