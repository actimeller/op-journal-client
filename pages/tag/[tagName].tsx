import { GetServerSideProps } from "next";

import moment from "moment";
import { declinations, opImageUrl } from "../../utils";
import { getArticles, getTags } from "../../api";

import Link from "../../components/Link";
import MainContainer from "../../components/MainContainer";
import Recommended from "../../components/Recommended";
import _ from "lodash";

interface TagListProps {
  tagLabel: string;
  cards: Nullable<ArticleResponse[]>;
}

export default function TagList({ tagLabel, cards }: TagListProps) {
  if (!cards) {
    return (
      <MainContainer>
        <section className="journal-headings-section">
          <div className="wrap">
            <div className="journal-headings">
              <p className="journal-headings__title">
                Error: No tag item found
              </p>
            </div>
          </div>
        </section>
      </MainContainer>
    );
  }
  return (
    <MainContainer>
      <section className="journal-headings-section">
        <div className="wrap">
          <div className="journal-headings">
            <p className="journal-headings__title">{tagLabel}</p>
            {cards
              .slice(0, 20)
              .map(
                ({
                  article_name: title,
                  article_id: id,
                  image_thumbail_url: imageUrl,
                  date,
                  time_to_read: timeToRead,
                }) => (
                  <Link
                    key={_.uniqueId()}
                    className="journal-headings__card"
                    to={`/article/${id}`}
                  >
                    <span
                      className="journal-headings__card-bg"
                      style={{
                        backgroundImage: `url(${opImageUrl(imageUrl)})`,
                      }}
                    />
                    <div className="journal-headings__card-data">
                      <div className="journal-story-data journal-story-data_shadow">
                        <div className="journal-story-data__item-shadow">
                          <span className="journal__hashtag journal__hashtag_transparent journal__hashtag_invert-mobile">
                            #{tagLabel}
                          </span>
                        </div>
                        <div className="journal-story-data__item-shadow">
                          <time className="journal__date">
                            <span className="journal__date-desktop">
                              {moment(date).format("DD MMMM YYYY")}
                            </span>
                            <span className="journal__date-mobile">
                              {moment(date).format("DD.MM.YY")}
                            </span>
                          </time>
                        </div>
                        <div className="journal-story-data__item-shadow">
                          <time className="journal__time">
                            <span className="journal__time-icon" /> {timeToRead}
                          </time>
                        </div>
                      </div>
                    </div>
                    <div className="journal-headings__card-title">{title}</div>
                    <span className="journal__btn-read desktop-only">
                      Прочесть
                    </span>
                  </Link>
                )
              )}
          </div>
        </div>
      </section>
      <Recommended cards={cards.slice(0, 10)} />
    </MainContainer>
  );
}

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const [tagsResponse, cardsResponse] = await Promise.all([
    getTags(),
    getArticles(),
  ]);
  const tagName = params?.tagName as string;

  const cardsFiltered =
    cardsResponse?.filter((card) => card.tagslug === tagName) || null;

  const { tag } = tagsResponse?.find((tag) => tag.tagslug === tagName) || {};
  const tagLabel = tag || null;

  return {
    props: {
      tagLabel,
      cards: cardsFiltered,
    },
  };
};
