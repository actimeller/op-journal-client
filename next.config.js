/** @type {import('next').NextConfig} */
module.exports = {
  // reactStrictMode: true,
  env: {
    // PASSWORD_PROTECT: process.env.ENVIRONMENT === 'staging',
    PASSWORD_PROTECT: true,
  },
  images: {
    domains: ["onlinepatent.ru"],
  },
};
