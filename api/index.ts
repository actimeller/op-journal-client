import { DataProp } from "editorjs-blocks-react-renderer";

const { NEXT_PUBLIC_API_URL } = process.env;

export const ENDPOINTS = {
  ROOT: {
    PATH: "/",
  },
  TAGS: {
    PATH: "/tags/view",
  },
  AUTHORS: {
    PATH: "/authors/view",
  },
  ARTICLE: {
    PATH: "/articles/view",
    DETAILS: {
      PATH: "/articles/view/",
      EDIT: "/articles/edit",
    },
    TOP: {
      PATH: "/articles/top/view",
    },
    LATEST_NEWS: {
      PATH: "/articles/latest_news",
    },
  },
  EDITOR: {
    IMAGE_UPLOAD: "/image/upload/",
    FETCH_URL: "/editor/fetchUrl",
  },
};

export const sendRequest = async (requestUrl: string, options?: any) => {
  try {
    const res = await fetch(NEXT_PUBLIC_API_URL + requestUrl, {
      method: options?.method || "POST",
      headers: {
        "Content-Type": "application/json;charset=utf-8",
        "Access-Control-Allow-Origin": "*",
      },
      mode: "cors",
      body: options?.body ? JSON.stringify(options.body) : undefined,
    });
    const {
      payload,
      response,
    }: {
      payload: any;
      response: string;
    } = await res.json();

    if (response === "success") {
      return payload;
    } else {
      throw response;
    }
  } catch (error) {
    console.log(
      `sendRequest - ${NEXT_PUBLIC_API_URL + requestUrl} error: ${error}`
    );
    return null;
  }
};

export const getTags = (): Promise<Nullable<TagResponse[]>> => {
  return sendRequest(ENDPOINTS.TAGS.PATH);
};

export const getAuthors = async (): Promise<Nullable<AuthorResponse[]>> => {
  return sendRequest(ENDPOINTS.AUTHORS.PATH);
};

export const getArticles = async (): Promise<Nullable<ArticleResponse[]>> => {
  return sendRequest(ENDPOINTS.ARTICLE.PATH);
};

export const getMainArticles = async (): Promise<
  Nullable<ArticleResponse[]>
> => {
  return sendRequest(ENDPOINTS.ARTICLE.TOP.PATH);
};

export const getArticleById = async (
  id: number
): Promise<Nullable<ArticleCombinedResponse>> => {
  const [articleResponse, articlesResponse] = await Promise.all([
    sendRequest(ENDPOINTS.ARTICLE.DETAILS.PATH + id),
    getArticles(),
  ]);
  const article = {
    ...articlesResponse?.find(
      (card) => card.article_id === articleResponse?.article_id
    ),
    ...articleResponse,
  };

  // todo: all articles should be in editor.js format
  try {
    article.content = JSON.parse(article.content);
  } catch (error) {
    article.content = {
      time: 1643814150920,
      version: "2.22.2",
      blocks: [
        {
          type: "paragraph",
          data: {
            text: article.content,
          },
        },
      ],
    };
  }

  return article;
};

export const getAuthorById = async (
  id: number
): Promise<Nullable<AuthorResponse>> => {
  const authors = await getAuthors();
  return authors == null
    ? null
    : authors.find((author) => parseInt(author.id) === id) || null;
};

export const getLatestNews = async (): Promise<Nullable<ArticleResponse[]>> => {
  return sendRequest(ENDPOINTS.ARTICLE.LATEST_NEWS.PATH);
};

//todo: server response should be in Editor.js format
export const prepareArticleContent = (content?: string | DataProp) => {
  if (typeof content === "string") {
    return {
      time: 1643814150920,
      version: "2.22.2",
      blocks: [
        {
          type: "paragraph",
          data: {
            text: content,
          },
        },
      ],
    };
  }
  return content;
};

export const editArticle = async (article: ArticleDetail) => {
  return sendRequest(ENDPOINTS.ARTICLE.DETAILS.EDIT, {
    body: article,
  });
};
